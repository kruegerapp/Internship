import numpy as np
from scipy.optimize import minimize
from sklearn.preprocessing import StandardScaler
from sklearn.utils import shuffle

class MoMLogisticRegression:
    def __init__(self, num_blocks=20, max_iter=100, tol=1e-4, random_state=None):
        self.num_blocks = num_blocks
        self.max_iter = max_iter
        self.tol = tol
        self.random_state = random_state
        self.coef_ = None
        self.scaler = None

    def _logistic_loss(self, w, X, y, sample_weight=None):
        if sample_weight is None:
            sample_weight = np.ones(X.shape[0])
        z = X.dot(w)
        log_likelihood = -sample_weight * (y * z - np.log(1 + np.exp(z)))
        return np.mean(log_likelihood)

    def _logistic_gradient(self, w, X, y, sample_weight=None):
        if sample_weight is None:
            sample_weight = np.ones(X.shape[0])
        z = X.dot(w)
        p = 1 / (1 + np.exp(-z))
        grad = np.dot(X.T, sample_weight * (p - y)) / X.shape[0]
        return grad

    def _median_of_means_gradient(self, w, X, y, sample_weight=None):
        if sample_weight is None:
            sample_weight = np.ones(X.shape[0])
        
        shuffled_indices = shuffle(np.arange(X.shape[0]), random_state=self.random_state)
        block_size = X.shape[0] // self.num_blocks
        block_gradients = []

        for i in range(self.num_blocks):
            start = i * block_size
            if i == self.num_blocks - 1:
                end = X.shape[0]
            else:
                end = start + block_size
            block_indices = shuffled_indices[start:end]
            
            # Extract the corresponding sample weights for the current block
            block_sample_weight = sample_weight[block_indices]
            
            block_gradient = self._logistic_gradient(w, X[block_indices], y[block_indices], sample_weight=block_sample_weight)
            block_gradients.append(block_gradient)

        median_gradient = np.median(block_gradients, axis=0)
        return median_gradient

    def fit(self, X, y, sample_weight=None):
        # Standardize features
        self.scaler = StandardScaler()
        X = self.scaler.fit_transform(X)
    
        # Add bias term as an extra column
        X = np.hstack([X, np.ones((X.shape[0],1))])
        
        # Initialize weights with the correct shape
        initial_w = np.zeros(X.shape[1])

        # Minimize the logistic loss
        options = {'maxiter': self.max_iter, 'disp': False}
        result = minimize(
            fun=self._logistic_loss,
            x0=initial_w,
            args=(X, y, sample_weight),
            method='L-BFGS-B',
            jac=self._median_of_means_gradient,
            options=options,
            tol=self.tol
        )

        # Update the model with the optimized parameters
        self.coef_ = result.x
        return self

    def predict_proba(self, X):
        X = self.scaler.transform(X)  # Standardize features
        X_with_bias = np.hstack([X, np.ones((X.shape[0], 1))])  # Add bias term
        z = X_with_bias.dot(self.coef_)  # Use the entire self.coef_
        p = 1 / (1 + np.exp(-z))
        return np.vstack([1-p, p]).T

    def predict(self, X):
        return (self.predict_proba(X)[:, 1] > 0.5).astype(int)
    
    def get_params(self, deep=True):
        # Return a dictionary of all parameters
        return {
            'num_blocks': self.num_blocks,
            'max_iter': self.max_iter,
            'tol': self.tol,
            'random_state': self.random_state
        }

    def set_params(self, **params):
        # Set parameters based on the input dictionary
        for parameter, value in params.items():
            setattr(self, parameter, value)
        return self
